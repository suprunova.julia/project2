package PO;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.SneakyThrows;
import org.testng.Assert;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selenide.*;

public class HandbagsPage {

  public MainPage clickLogo() {
    $(byClassName("logoImage")).shouldBe(enabled).click();
    return page(MainPage.class);
  }

  public HandbagsPage checkLogoUrl() {
    $(".grey.store-name")
        .shouldHave(attribute("href", Configuration.baseUrl));
    return this;
  }

  public void sortProductsByNameOption() {

    $("#filter").shouldHave(enabled).selectOptionByValue("item-name");
  }

  public List<String> getProductsNames() {
    return $$x("//*[@id='productsContainer']/*")
        .shouldBe(CollectionCondition.sizeGreaterThan(0))
        .stream()
        .map(x -> x.getAttribute("item-name"))
        .collect(Collectors.toList());
  }

  private void closeCookieDialog(){
    if($("[role='dialog']").isDisplayed()){
      $("[role='dialog'] .cc-compliance").click();
    }
  }

  @SneakyThrows
  public HandbagsPage addFirstProductToCart(){
    closeCookieDialog();

    ElementsCollection products = $$(".addToCart");

    long maxWaitTime = 3000L;
    long startTime = System.currentTimeMillis();

    while (products.stream().noneMatch(SelenideElement::isDisplayed)){
      Thread.sleep(100L);
      products = $$(".addToCart");

      long endTime = System.currentTimeMillis();
      Assert.assertTrue((endTime - startTime) < maxWaitTime, "Waiting time exceeded");
    }

    products.first().click();
    return this;
  }

  public HandbagsPage cartProductCounterShouldBeEqual(int productsAmount){
    $("#miniCartSummary").shouldHave(text(String.valueOf(productsAmount)));
    return this;
  }

  @SneakyThrows
  public CartPage clickCheckout(){
    $x("//span[contains(text(), ' Shopping cart')]").click();
    $("#miniCartDetails .cart-footer .pull-right").click();
    return page(CartPage.class);
  }
}
