package PO;

import com.codeborne.selenide.Configuration;
import lombok.Getter;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

@Getter
public class MainPage {
  public MainPage checkLogoUrl() {
    $(".grey.store-name")
        .shouldHave(attribute("href", Configuration.baseUrl));
    return this;
  }

  public void clickLogo() {

    $(byClassName("logoImage")).shouldBe(enabled).click();
  }

  public HandbagsPage selectHandbagsProduct() {
    $(byText("Products")).shouldHave(exist).hover();
    $(byText("Handbags")).shouldHave(enabled).click();
    return page(HandbagsPage.class);
  }

  public LaptopBagsPage selectLaptopBagsProduct() {
    $(byText("Products")).shouldHave(exist).hover();
    $(byText("Laptop Bags")).shouldHave(enabled).click();
    return page(LaptopBagsPage.class);
  }

  public BeachBagsPage selectBeachBagsProduct() {
    $(byText("Products")).shouldHave(exist).hover();
    $(byText("Beach bags")).shouldHave(enabled).click();
    return page(BeachBagsPage.class);
  }

  public BagsPage selectBagsProduct() {
    $(byText("Products")).shouldHave(exist).hover();
    $(byText("Bags")).shouldHave(enabled).click();
    return page(BagsPage.class);
  }

  public ContactUsPage clickContactUs() {
    $x("//*[text()='Contact us']").shouldBe(enabled).click();
    return page(ContactUsPage.class);
  }

  public RegistrationPage clickOnRegisterInFooter(){
    $x("//div[contains(@class,'footer-wrapper')]//a[text()='Register']").click();
    return page(RegistrationPage.class);
  }
}