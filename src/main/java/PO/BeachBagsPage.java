package PO;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.SneakyThrows;
import org.testng.Assert;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selenide.*;

public class BeachBagsPage {
  public MainPage clickLogo() {
    $(byClassName("logoImage")).shouldBe(enabled).click();
    return page(MainPage.class);
  }

  public BeachBagsPage checkLogoUrl() {
    $(".grey.store-name")
        .shouldHave(attribute("href", Configuration.baseUrl));
    return this;
  }

  public void sortProductsByNameOption() {
    $("#filter").shouldHave(enabled).selectOptionByValue("item-name");
  }

  public List<String> getProductsNames() {
    return $$x("//*[@id='productsContainer']/*")
        .shouldBe(CollectionCondition.sizeGreaterThan(0))
        .stream()
        .map(x -> x.getAttribute("item-name"))
        .collect(Collectors.toList());
  }

  @SneakyThrows
  public List<String> getNamesOfAllVisibleProducts(){
    ElementsCollection products = $$(".product-content a h3");

    long maxWaitTime = 3000L;
    long startTime = System.currentTimeMillis();
    while (products.stream().noneMatch(SelenideElement::isDisplayed)){
      Thread.sleep(100L);
      products = $$(".product-content a h3");

      long endTime = System.currentTimeMillis();
      Assert.assertTrue((endTime - startTime) < maxWaitTime, "Waiting time exceeded");
    }

    return products.stream()
            .filter(SelenideElement::isDisplayed)
            .map(SelenideElement::text)
            .collect(Collectors.toList());
  }
}
