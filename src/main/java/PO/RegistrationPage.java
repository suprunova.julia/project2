package PO;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class RegistrationPage {
  public RegistrationPage fillFirstName(String firstname){
    $("[name='billing.firstName']").sendKeys(firstname);
    return this;
  }

  public RegistrationPage fillLastName(String lastname){
    $("[name='billing.lastName']").sendKeys(lastname);
    return this;
  }

  public RegistrationPage chooseCountry(){
    $("[name='billing.country']").selectOption(1);
    return this;
  }

  public RegistrationPage fillState(String state){
    $("[name='billing.stateProvince'").sendKeys(state);
    return this;
  }

  public RegistrationPage fillEmail(String email){
    $("#emailAddress").sendKeys(email);
    return this;
  }

  public RegistrationPage fillPassword(String password){
    $("#password").sendKeys(password);
    return this;
  }

  public RegistrationPage repeatPassword(String password){
    $("#passwordAgain").sendKeys(password);
    return this;
  }

  public DashboardPage clickCreateAccount(){
    $(".login-btn").click();
    return page(DashboardPage.class);
  }
}
