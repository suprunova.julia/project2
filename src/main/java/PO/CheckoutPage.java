package PO;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class CheckoutPage {
  public CheckoutPage fillFirstName(String firstName){
    $("[name='customer.billing.firstName']").sendKeys(firstName);
    return this;
  }

  public CheckoutPage fillLastName(String lastName){
    $("[name='customer.billing.lastName']").sendKeys(lastName);
    return this;
  }

  public CheckoutPage fillStreetAddress(String address){
    $("[name='customer.billing.address']").sendKeys(address);
    return this;
  }

  public CheckoutPage fillCity(String city){
    $("[name='customer.billing.city']").sendKeys(city);
    return this;
  }

  public CheckoutPage chooseCountry(){
    $("[name='customer.billing.country']").selectOption(1);
    return this;
  }

  public CheckoutPage fillState(String state){
    $("#billingStateProvince").sendKeys(state);
    return this;
  }

  public CheckoutPage fillPostalCode(String postalCode){
    $("#billingPostalCode").sendKeys(postalCode);
    return this;
  }

  public CheckoutPage fillEmail(String email){
    $("[name='customer.emailAddress']").sendKeys(email);
    return this;
  }

  public CheckoutPage fillPhoneNumber(String phoneNumbers){
    $("[name='customer.billing.phone']").sendKeys(phoneNumbers);
    return this;
  }

  public SuccessfulOrderPage clickSubmit(){
    $("#formErrorMessage").shouldHave(text("The order can be completed"));
    $("#submitOrder").click();
    return page(SuccessfulOrderPage.class);
  }
}
