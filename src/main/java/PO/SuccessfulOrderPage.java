package PO;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class SuccessfulOrderPage {
  public SuccessfulOrderPage successfulMessageShouldBeVisible(){
    $("#main-content h1").shouldHave(text("Order completed"));
    return this;
  }
}
