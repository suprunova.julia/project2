package PO;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class CartPage {
  public CheckoutPage clickProceedToCheckout(){
    $(byText("Proceed to checkout")).click();
    return page(CheckoutPage.class);
  }
}
