package PO;

import com.codeborne.selenide.Configuration;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$x;

public class DashboardPage {
  public DashboardPage checkMyAccountLink(){
    $x("//div[contains(@class,'footer-wrapper')]//a[text()='My Account']")
            .shouldHave
                    (attribute("href", Configuration.baseUrl + "customer/account.html"));
    return this;
  }

  public DashboardPage checkLogoutLink(){
    $x("//div[contains(@class,'footer-wrapper')]//a[text()='Logout']")
            .shouldHave
                    (attribute("href", Configuration.baseUrl + "customer/logout"));
    return this;
  }
}
