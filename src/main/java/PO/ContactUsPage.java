package PO;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class ContactUsPage {
  public ContactUsPage fillName(String name) {
    $("#name").should(Condition.empty).sendKeys(name);
    return this;
  }

  public ContactUsPage fillEmail(String email) {
    $("#email").should(Condition.empty).sendKeys(email);
    return this;
  }

  public ContactUsPage fillSubject(String subject) {
    $("#subject").should(Condition.empty).sendKeys(subject);
    return this;
  }

  public ContactUsPage fillComment(String comment) {
    $("#comment").shouldHave(Condition.empty).sendKeys(comment);
    return this;
  }

  public ContactUsPage sendFeedback() {
    $("#submitContact").shouldBe(Condition.exist).click();
    return this;
  }

  public void assertSuccessSend() {
    $(byText("Your message has been sent")).shouldHave(Condition.visible);
  }

  public void assertUnsent() {
    $(byText("Your message has been sent")).shouldHave(Condition.hidden);
  }

  public ContactUsPage checkLogoUrl() {
    $(".grey.store-name")
        .shouldHave(attribute("href", Configuration.baseUrl));
    return this;
  }

  public MainPage clickLogo() {
    $(byClassName("logoImage")).shouldBe(enabled).click();
    return page(MainPage.class);
  }
}
