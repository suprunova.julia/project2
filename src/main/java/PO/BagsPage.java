package PO;

import com.codeborne.selenide.Configuration;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class BagsPage {
  public MainPage clickLogo() {
    $(byClassName("logoImage")).shouldBe(enabled).click();
    return page(MainPage.class);
  }
  public BagsPage checkLogoUrl() {
    $(".grey.store-name")
        .shouldHave(attribute("href", Configuration.baseUrl));
    return this;
  }
}
