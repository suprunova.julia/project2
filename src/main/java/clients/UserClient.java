package clients;

import com.fasterxml.jackson.core.type.TypeReference;
import common.ResponseWrapper;
import dto.*;
import lombok.SneakyThrows;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.testng.collections.Lists;

import java.net.URI;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

public class UserClient extends BaseClient {
  private String authHeader;

  @SneakyThrows
  public ResponseWrapper<UserDTO> register(RegistrationDTO registrationDto) {
    HttpPost httpPost = new HttpPost(BASE_PATH + "/api/v1/customer/register");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(registrationDto)));
    CloseableHttpResponse httpResponse = client.execute(httpPost);
    return new ResponseWrapper<>(httpResponse, new TypeReference<>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<UserDTO> login(LoginDTO loginDTO) {
    HttpPost httpPost = new HttpPost(BASE_PATH + "/api/v1/customer/login");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(loginDTO)));
    CloseableHttpResponse httpResponse = client.execute(httpPost);

    authHeader = Base64
        .getEncoder()
        .encodeToString((loginDTO.getUsername() + ":" + loginDTO.getPassword()).getBytes());
    return new ResponseWrapper<>(httpResponse, new TypeReference<>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<AccountSummaryDTO> getUserInfo(String userName) {
    HttpGet httpGet = new HttpGet(BASE_PATH + "/shop/customer/accountSummary.json");
    httpGet.setHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, "Basic " + authHeader));
    URI uri = new URIBuilder(httpGet.getURI())
        .addParameter("userName", userName)
        .build();
    httpGet.setURI(uri);
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<CategoryDTO> getCategoriesProductInfo(String category) {
    HttpGet httpGet = new HttpGet(BASE_PATH + "/services/public/products/page/0/0/DEFAULT/en/" + category);
    CloseableHttpResponse httpResponse = client.execute(httpGet);
    return new ResponseWrapper<>(httpResponse, new TypeReference<>() {
    });
  }

  @SneakyThrows
  public ResponseWrapper<String> contactMessage(ContactMessageDTO contactMessageDTO){
    List<NameValuePair> pairs = Lists.newArrayList(
            new BasicNameValuePair("name", contactMessageDTO.getName()),
            new BasicNameValuePair("email", contactMessageDTO.getEmail()),
            new BasicNameValuePair("subject", contactMessageDTO.getSubject()),
            new BasicNameValuePair("comment", contactMessageDTO.getComments()));

    HttpPost httpPost = new HttpPost(BASE_PATH + "/shop/store/DEFAULT/contact");
    httpPost.setEntity(new UrlEncodedFormEntity(pairs));
    CloseableHttpResponse httpResponse = client.execute(httpPost);
    return new ResponseWrapper<>(httpResponse, new TypeReference<>() {
    });
  }

  @SneakyThrows
  public List<ProductDTO> getProductsForAddingToCart(int productsAmount){
    List<String> categoriesList = Lists.newArrayList("handbags", "beach-bags", "laptop-bags", "bags");

    List<ProductDTO> products = new ArrayList<>();

    for (String category : categoriesList) {
      while (products.size() < productsAmount){
        CategoryDTO categoryDTO = getCategoriesProductInfo(category).parseContent();
        for(ProductDTO product : categoryDTO.getProducts()){
          products.add(product);
          if(products.size() >= productsAmount ){
            break;
          }
        }
      }
    }

    return products;
  }

  @SneakyThrows
  public ResponseWrapper<CartDTO> addItemToCart(ProductInCartDTO productInCartDTO){
    HttpPost httpPost = new HttpPost(BASE_PATH + "/shop/cart/addShoppingCartItem");
    httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
    httpPost.setEntity(new StringEntity(mapper.writeValueAsString(productInCartDTO)));

    CloseableHttpResponse httpResponse = client.execute(httpPost);
    return new ResponseWrapper<>(httpResponse, new TypeReference<>() {
    });
  }

  public List<String> getProductsNameForCategory(String category){
    return getCategoriesProductInfo(category).parseContent()
            .getProducts().stream()
            .map(product -> product.getDescription().getName())
            .collect(Collectors.toList());
  }
}


