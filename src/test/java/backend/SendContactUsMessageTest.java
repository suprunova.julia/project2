package backend;

import clients.UserClient;
import dto.ContactMessageDTO;
import org.testng.annotations.Test;

public class SendContactUsMessageTest {
  UserClient userClient = new UserClient();

  @Test(testName = "Send \"Contact us\" message without name (negative)")
  public void contactUsWithoutNameTest(){
    ContactMessageDTO contactMessageDTO = ContactMessageDTO
            .builder()
            .name(null)
            .email("testContactEmail")
            .subject("testContactSubject")
            .comments("testContactComment")
            .build();

    userClient.contactMessage(contactMessageDTO).statusCodeIs(400);
  }

  @Test(testName = "Send \"Contact us\" message without email (negative)")
  public void contactUsWithoutEmailTest(){
    ContactMessageDTO contactMessageDTO = ContactMessageDTO
            .builder()
            .name("testContactName")
            .email(null)
            .subject("testContactSubject")
            .comments("testContactComment")
            .build();

    userClient.contactMessage(contactMessageDTO).statusCodeIs(400);
  }

  @Test(testName = "Send \"Contact us\" message without subject (negative)")
  public void contactUsWithoutSubjectTest(){
    ContactMessageDTO contactMessageDTO = ContactMessageDTO
            .builder()
            .name("testContactName")
            .email("testContactEmail")
            .subject(null)
            .comments("testContactComment")
            .build();

    userClient.contactMessage(contactMessageDTO).statusCodeIs(400);
  }

  @Test(testName = "Send \"Contact us\" message without comments (negative)")
  public void contactUsWithoutCommentsTest(){
    ContactMessageDTO contactMessageDTO = ContactMessageDTO
            .builder()
            .name("contactTestName")
            .email("testContactEmail")
            .subject("testContactSubject")
            .comments(null)
            .build();

    userClient.contactMessage(contactMessageDTO).statusCodeIs(400);
  }

  @Test(testName = "Send valid \"Contact us\" message (positive)")
  public void contactUsValidTest(){
    ContactMessageDTO contactMessageDTO = ContactMessageDTO
            .builder()
            .name("testContactName")
            .email("testContactEmail")
            .subject("testContactSubject")
            .comments("testContactComment")
            .build();

    userClient.contactMessage(contactMessageDTO).statusCodeIs(200);
  }
}
