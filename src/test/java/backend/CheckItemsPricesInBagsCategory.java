package backend;

import clients.UserClient;
import dto.CategoryDTO;
import dto.ProductDTO;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

public class CheckItemsPricesInBagsCategory {
  private final UserClient userClient = new UserClient();

  @Test(testName = "Check that all items in Bags category have price(positive)")
  public void CheckItemsPrices() {
    CategoryDTO categoryDTO = userClient
        .getCategoriesProductInfo("bags")
        .statusCodeIs(200)
        .parseContent();

    List<Double> priceList = categoryDTO
        .getProducts()
        .stream()
        .map(ProductDTO::getPrice)
        .collect(Collectors.toList());

    Assert.assertFalse(priceList.contains(null), "product without price");
  }
}
