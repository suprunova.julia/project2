package backend;

import clients.UserClient;
import dto.CartDTO;
import dto.ProductDTO;
import dto.ProductInCartDTO;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ValidateCartTotalPriceTest {
  UserClient userClient = new UserClient();

  @Test(testName = "Add two items to cart and validate total price")
  public void cartTotalPriceTest(){
    int productsAmount = 2;
    List<ProductDTO> products = userClient.getProductsForAddingToCart(productsAmount);
    Assert.assertEquals(productsAmount, products.size(), "Can't find enough products, to validate cart total price");

    ProductInCartDTO cartProductInfoDTO = ProductInCartDTO.builder()
            .productId(products.get(0).getProductId())
            .orderedQuantity(1)
            .build();

    CartDTO cartDTO = userClient.addItemToCart(cartProductInfoDTO).parseContent();
    cartProductInfoDTO.setCode(cartDTO.getCode());

    for(int i=1; i<products.size(); i++){
      cartProductInfoDTO.setProductId(products.get(i).getProductId());
      cartDTO = userClient.addItemToCart(cartProductInfoDTO).parseContent();
    }

    Double calculatedTotalPrice = products.stream()
            .mapToDouble(ProductDTO::getPrice)
            .sum();

    Double cartTotalPrice = Double.parseDouble(cartDTO.getUnparsedTotalPrice().substring(1));

    Assert.assertEquals(calculatedTotalPrice, cartTotalPrice, "Invalid cart total price");
  }
}
