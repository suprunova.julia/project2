package backend;

import clients.UserClient;
import dto.AccountSummaryDTO;
import dto.BillingDTO;
import dto.LoginDTO;
import dto.RegistrationDTO;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.ThreadLocalRandom;


public class LoginAndGetAccountSummaryTest {
  private final UserClient userClient = new UserClient();
  private final String emailAddress = "j1" + ThreadLocalRandom.current().nextInt() + "@gmail.com";
  private final String password = "p" + System.currentTimeMillis();
  private final String firstName = "f" + System.currentTimeMillis();
  private final String lastName = "l" + System.currentTimeMillis();

  @BeforeClass
  public void registerAccount() {
    String country = "UA";
    String zone = "CA";
    RegistrationDTO registrationDto = RegistrationDTO
        .builder()
        .billing(BillingDTO.builder().country(country).zone(zone).build())
        .emailAddress(emailAddress)
        .firstName(firstName)
        .lastName(lastName)
        .password(password)
        .passwordConfirm(password)
        .build();
    userClient
        .register(registrationDto)
        .statusCodeIs(200);
  }

  @Test(testName = "Login and get accountSummary(positive)")
  public void loginAndGetAccountSummary() {
    LoginDTO loginDTO = LoginDTO
        .builder()
        .username(emailAddress)
        .password(password)
        .build();
    userClient
        .login(loginDTO)
        .statusCodeIs(200);

    AccountSummaryDTO accountSummaryDTO = userClient
        .getUserInfo(emailAddress)
        .statusCodeIs(200)
        .parseContent();
    SoftAssert softAssert = new SoftAssert();
    softAssert.assertEquals(accountSummaryDTO.getEmailAddress(), emailAddress);
    softAssert.assertEquals(accountSummaryDTO.getFirstName(), firstName);
    softAssert.assertEquals(accountSummaryDTO.getLastName(), lastName);
    softAssert.assertAll();
  }

  @Test(testName = "Login and get accountSummary(negative)")
  public void loginUnregistered() {
    String customUsername = "a" + System.currentTimeMillis();
    String customPassword = "8888%";

    LoginDTO loginDTO = LoginDTO
        .builder()
        .username(customUsername)
        .password(customPassword)
        .build();
    userClient
        .login(loginDTO)
        .statusCodeIs(401)
        .parseContent();

    AccountSummaryDTO accountSummaryDTO = userClient
        .getUserInfo(customUsername)
        .statusCodeIs(401)
        .parseContent();
    Assert.assertNull(accountSummaryDTO.getEmailAddress());
  }

  @Test(testName = "Login and get accountSummary(negative)")
  public void GetAccountSummaryUnauthorized() {
    AccountSummaryDTO accountSummaryDTO = userClient
        .getUserInfo(emailAddress)
        .statusCodeIs(401)
        .parseContent();
    Assert.assertNull(accountSummaryDTO.getEmailAddress());
  }
}
