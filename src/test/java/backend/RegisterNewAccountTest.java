package backend;

import clients.UserClient;
import dto.BillingDTO;
import dto.RegistrationDTO;
import dto.UserDTO;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.ThreadLocalRandom;

public class RegisterNewAccountTest {

  private final UserClient userClient = new UserClient();
  private final String firstName = "f" + System.currentTimeMillis();
  private final String lastName = "l" + System.currentTimeMillis();
  private final String pass = "P" + System.currentTimeMillis();
  private final String country = "UA";
  private final String zone = "CA";

  @Test(testName = "Register new account(positive)")
  public void registerClient() {
    String emailAddress = "j1" + ThreadLocalRandom.current().nextInt() + "@gmail.com";
    RegistrationDTO registrationDTO = RegistrationDTO
        .builder()
        .billing(BillingDTO.builder().country(country).zone(zone).build())
        .emailAddress(emailAddress)
        .firstName(firstName)
        .lastName(lastName)
        .password(pass)
        .passwordConfirm(pass)
        .build();
    UserDTO user = userClient
        .register(registrationDTO)
        .statusCodeIs(200)
        .parseContent();
    SoftAssert softAssert = new SoftAssert();
    softAssert.assertTrue(user.getId() > 0);
    softAssert.assertNotNull(user.getToken());
    softAssert.assertAll();
  }

  @Test(testName = "Register new account(negative)")
  public void registerClientWithNullEmail() {
    RegistrationDTO registrationDto = RegistrationDTO
        .builder()
        .billing(BillingDTO.builder().country(country).zone(zone).build())
        .emailAddress(null)
        .firstName(firstName)
        .lastName(lastName)
        .password(pass)
        .passwordConfirm(pass)
        .build();
    UserDTO user = userClient
        .register(registrationDto)
        .statusCodeIs(500)
        .parseContent();
    SoftAssert softAssert = new SoftAssert();
    softAssert.assertEquals(user.getId(), 0);
    softAssert.assertAll();
  }
}
