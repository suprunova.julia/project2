package frontend;

import PO.BeachBagsPage;
import clients.UserClient;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

public class ProductsNameEqualityTest extends BaseWebTest{

  @Test(testName = "Beach bags category items on UI have the same names as in the response from the back end.")
  public void checkNamesEquality(){
    BeachBagsPage beachBagsPage = mainPage.selectBeachBagsProduct();

    List<String> frontendProductNames = beachBagsPage.getNamesOfAllVisibleProducts();
    Collections.sort(frontendProductNames);

    UserClient publicClient = new UserClient();
    List<String> backendProductNames = publicClient.getProductsNameForCategory("beach-bags");
    Collections.sort(backendProductNames);

    Assert.assertEquals(frontendProductNames, backendProductNames);
  }
}
