package frontend;

import org.testng.annotations.Test;

public class CheckFormAllFieldsFilledTest extends BaseWebTest {

  @Test(testName = "Check that \"Contact us\" form can be submitted only when all fields are filled (positive)")
  public void checkAllFieldsFiled() {
    mainPage
        .clickContactUs()
        .fillName("a")
        .fillEmail("a@gmail.com")
        .fillSubject("greeting")
        .fillComment("Hello!")
        .sendFeedback()
        .assertSuccessSend();
  }

  @Test(testName = "Check that \"Contact us\" form can be submitted only when all fields are filled (negative)")
  public void sendWithoutName() {
    mainPage
        .clickContactUs()
        .fillEmail("a@gmail.com")
        .fillSubject("greeting")
        .fillComment("Hello!")
        .sendFeedback()
        .assertUnsent();
  }

  @Test(testName = "Check that \"Contact us\" form can be submitted only when all fields are filled (negative)")
  public void sendWithoutEmail() {
    mainPage
        .clickContactUs()
        .fillName("a")
        .fillSubject("greeting")
        .fillComment("Hello!")
        .sendFeedback()
        .assertUnsent();
  }

  @Test(testName = "Check that \"Contact us\" form can be submitted only when all fields are filled (negative)")
  public void sendWithoutSubject() {
    mainPage
        .clickContactUs()
        .fillName("a")
        .fillEmail("a@gmail.com")
        .fillComment("Hello!")
        .sendFeedback()
        .assertUnsent();
  }

  @Test(testName = "Check that \"Contact us\" form can be submitted only when all fields are filled (negative)")
  public void sendWithoutComment() {
    mainPage
        .clickContactUs()
        .fillName("a")
        .fillEmail("a@gmail.com")
        .fillSubject("greeting")
        .sendFeedback()
        .assertUnsent();
  }

}
