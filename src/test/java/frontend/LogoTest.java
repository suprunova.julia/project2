package frontend;

import org.testng.annotations.Test;


public class LogoTest extends BaseWebTest {

  @Test(testName = "Check that logo leads to 'main' page from 'main' page (positive)")
  public void checkLogoLeadsToMainPageFromMainPage() {
    mainPage.checkLogoUrl().checkLogoUrl().clickLogo();
  }

  @Test(testName = "Check that logo leads to 'main' page from 'handbags' page (positive)")
  public void checkLogoLeadsToMainPageFromHandBagsPage() {
    mainPage.selectHandbagsProduct().checkLogoUrl().clickLogo();
  }

  @Test(testName = "Check that logo leads to 'main' page from 'bags' page (positive)")
  public void checkLogoLeadsToMainPageFromBagsPage() {
    mainPage.selectBagsProduct().checkLogoUrl().clickLogo();
  }

  @Test(testName = "Check that logo leads to 'main' page from 'beach-bags' page (positive)")
  public void checkLogoLeadsToMainPageFromBeachBagsPage() {
    mainPage.selectBeachBagsProduct().checkLogoUrl().clickLogo();
  }

  @Test(testName = "Check that logo leads to 'main' page from 'laptop-bags' page (positive)")
  public void checkLogoLeadsToMainPageFromLaptopBagsPage() {
    mainPage.selectLaptopBagsProduct().checkLogoUrl().clickLogo();
  }

  @Test(testName = "Check that logo leads to 'main' page from 'contact-us' page (positive)")
  public void checkLogoLeadsToMainPageFromContactUsPage() {
    mainPage.clickContactUs().checkLogoUrl().clickLogo();
  }

}
