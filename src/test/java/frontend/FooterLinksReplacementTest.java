package frontend;

import PO.RegistrationPage;
import org.testng.annotations.Test;

public class FooterLinksReplacementTest extends BaseWebTest {

  @Test(testName = "Check link replacement in footer after login")
  public void checkLinkReplacement(){
    String email = "email" + System.currentTimeMillis() + "@gmail.com";
    String password = "123456789";
    RegistrationPage registrationPage = mainPage
        .clickOnRegisterInFooter()
        .fillFirstName("FirstName" + System.currentTimeMillis())
        .fillLastName("LastName" + System.currentTimeMillis())
        .chooseCountry()
        .fillState("State" + System.currentTimeMillis())
        .fillEmail(email)
        .fillPassword(password)
        .repeatPassword(password);

    registrationPage
        .clickCreateAccount()
        .checkMyAccountLink()
        .checkLogoutLink();
  }
}
