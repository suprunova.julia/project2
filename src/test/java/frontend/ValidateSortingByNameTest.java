package frontend;


import PO.BeachBagsPage;
import PO.HandbagsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.sleep;

public class ValidateSortingByNameTest extends BaseWebTest {

  @Test(testName = "Validate handBagsPage sorting by name (positive)")
  public void validateHandbagsSortingByName() {
    HandbagsPage handBagsPage = mainPage.selectHandbagsProduct();
    List<String> defaultSorted = handBagsPage.getProductsNames();
    handBagsPage.sortProductsByNameOption();
    sleep(4000);
    List<String> sortedByNames = handBagsPage.getProductsNames();
    Assert.assertEquals(defaultSorted.stream().sorted().collect(Collectors.toList()), sortedByNames);
  }

  @Test(testName = "Validate beachBagsPage sorting by name (positive)")
  public void validateBeachBagsSortingByName() {
    BeachBagsPage beachBagsPage = mainPage.selectBeachBagsProduct();
    List<String> defaultSorted = beachBagsPage.getProductsNames();
    beachBagsPage.sortProductsByNameOption();
    sleep(4000);
    List<String> sortedByNames = beachBagsPage.getProductsNames();
    Assert.assertEquals(defaultSorted.stream().sorted().collect(Collectors.toList()), sortedByNames);
  }
}
