package frontend;

import PO.CartPage;
import PO.CheckoutPage;
import PO.HandbagsPage;
import org.testng.annotations.Test;

public class CompleteOrderWithHandbagTest extends BaseWebTest {

  @Test(testName = "Complete order of item from Handbags category")
  public void completeOrder() {
    HandbagsPage handbagsPage = mainPage
        .selectHandbagsProduct()
        .addFirstProductToCart()
        .cartProductCounterShouldBeEqual(1);

    CartPage cartPage = handbagsPage.clickCheckout();

    CheckoutPage checkoutPage = cartPage
        .clickProceedToCheckout()
        .fillFirstName("FirstName" + System.currentTimeMillis())
        .fillLastName("LastName" + System.currentTimeMillis())
        .fillStreetAddress("Address" + System.currentTimeMillis())
        .fillCity("City" + System.currentTimeMillis())
        .chooseCountry()
        .fillState("State" + System.currentTimeMillis())
        .fillPostalCode(String.valueOf(System.currentTimeMillis()))
        .fillEmail("email" + System.currentTimeMillis() + "@gmail.com")
        .fillPhoneNumber(String.valueOf(System.currentTimeMillis()));

    checkoutPage
        .clickSubmit()
        .successfulMessageShouldBeVisible();
  }
}
