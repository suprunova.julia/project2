package frontend;

import PO.MainPage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class BaseWebTest {
  protected MainPage mainPage;

  @BeforeSuite
  public void setUp() {
    Configuration.baseUrl = "http://167.71.33.250:8080/shop/";
    Configuration.timeout = 5000L;
    Selenide.open();
    WebDriverRunner.getWebDriver().manage().window().maximize();
  }

  @BeforeMethod
  public void open() {
    Selenide.open("");
    mainPage = new MainPage();
  }

  @AfterMethod
  public void close() {
    Selenide.closeWebDriver();
  }
}