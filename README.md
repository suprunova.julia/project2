# Project2

| Scenario | Type |
| ------ | ------ |
| Register new account | Backend |
| Send "Contact us" message | Backend |
| Login and get accountSummary | Backend |
| Add two items to cart and validate total price | Backend |
| Check that all items in Bags category have price| Backend |
| Check that Beach bags category items on UI have the same names as in the response from the back end.| Combined |
| Check that logo leads to main page| Frontend |
| Complete order of item from Handbags category | Frontend |
| Validate sorting by name | Frontend |
|Check link replacement in footer after login | Frontend |
| Check that "Contact us" form can be submitted only when all fields are filled | Frontend |





## Running test instruction:
1. Clone repository
```shell
git clone https://gitlab.com/suprunova.julia/project2.git
```
2. Move to project directory
```shell
cd project2
```
3. Choose test-type to run:
   
    3.1 Run all tests:
   ```shell
    mvn clean test
    mvn allure:serve
   ```

   3.2 Run backend tests:
   ```shell
    mvn clean test -P Backend
    mvn allure:serve
   ```

   3.3 Run frontend tests:
   ```shell
    mvn clean -P Frontend
    mvn allure:serve
   ```
   ```shell
   P.S. Clean allure-results folder before run allure
   ```
